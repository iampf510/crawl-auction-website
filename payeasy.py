#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import Parser

item_urls = []
urls = []
item_temp = open('payeasy_item_temp','w',1)
item_data = open('payeasy_item_data','w',1)
#page_urls = []
page_urls = ['http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29652?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29654?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29655?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29656?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/30704?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29658?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29989?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29657?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29659?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29660?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29663?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29662?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29652?page=1&sort=2&direction=1',
		'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29661?page=1&sort=2&direction=1']


f = open('payeasy_page_temp','w',1)
f2 = open('payeasy_store_temp','w',1)

dup = 0

def CompleteURL(url):
	
	url = url[6:-1]
	if 'http://member.payeasy.com.tw' not in url:
		url =  'http://member.payeasy.com.tw' + url
	if '&amp;' in url:
		url = url.replace('&amp;','&')
	if '"' in url:
		url = url.split('"')[0]
	if '#' in url:
		return None
	return url

#先抓取第一頁的所有分類, 再來multi-thread
def PayeasyPageInitial(url):
	#data = GetPageInfo.FetchWebPage(url)
	'''
	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)
	for i in m1:
		url = CompleteURL(i)
		if url is not None and url not in page_urls:
			if 'member.payeasy.com.tw' in url:
				print url
				page_urls.append(url)
	'''
	page_urls = ['http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29652?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29654?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29655?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29656?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/30704?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29658?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29989?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29657?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29659?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29660?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29663?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29662?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29652?page=1&sort=2&direction=1',
			'http://member.payeasy.com.tw/EcCategoryV2/Product/ProductList/29661?page=1&sort=2&direction=1']

def PayeasyFetchPage(url):

	dic, data = Parser.PayeasyPageParser(url)
	global dup
	if dic != None:
		for i in dic:
			#因為payeasy有許多相同網址的網頁, 差別在於商品圖片
			if i['img_url'] not in item_urls or i['url'] not in urls:
				item_urls.append(i['img_url'])
				urls.append(i['url'])
				item_temp.write(url+'\n')
			
				item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
				item_data.write(item)
			else:
				 dup += 1

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []

	for i in m1:
		url = CompleteURL(i)
		if url is not None and url not in page_urls:
			if 'member.payeasy' in url and '?page=' in url:
				if url not in page_urls:
					page_urls.append(url)
					do_url.append(url)
					f.write(url+'\n')

	for url in do_url:
		PayeasyFetchPage(url)



class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
				
			PayeasyFetchPage(url)
			self.queue.task_done()	


def PayeasyPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(10):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()
	url = 'http://www.payeasy.com.tw/index.shtml'	

	#PayeasyPageInitial(url)
	PayeasyPageSpider()

	#GoHappyItemSpider()
	f.close()
	f2.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'
	print 'dup =',dup