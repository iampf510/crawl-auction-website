#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
#import PCHomeItemParser
import Parser
import Bucket

buk_num = 100

item_urls = [] 
item_temp = open('gomaji_item_temp','w',1)
item_data = open('gomaji_item_data','w',1)
page_urls = [] 

f = open('gomaji_page_temp','w',1)
error_log = open('gomaji_error','w',1)

page_urls = ['http://www.gomaji.com/Taipei',
		'http://www.gomaji.com/Taoyuan',
		'http://www.gomaji.com/Hsinchu',
		'http://www.gomaji.com/Miaoli',
		'http://www.gomaji.com/Taichung',
		'http://www.gomaji.com/Changhua',
		'http://www.gomaji.com/Nantou',
		'http://www.gomaji.com/Yunlin',
		'http://www.gomaji.com/Chiayi',
		'http://www.gomaji.com/Tainan',
		'http://www.gomaji.com/Kaohsiung',
		'http://www.gomaji.com/Pingtung',
		'http://www.gomaji.com/Taitung',
		'http://www.gomaji.com/Hualien',
		'http://www.gomaji.com/Yilan',
		'http://www.gomaji.com/Keelung',
		'http://www.gomaji.com/Taiwan',
		'http://www.gomaji.com/travel.php',
		'http://www.gomaji.com/instant.php?city=Taipei',
		'http://www.gomaji.com/instant.php?city=Taoyuan',
		'http://www.gomaji.com/instant.php?city=Hsinchu',
		'http://www.gomaji.com/instant.php?city=Taichung',
		'http://www.gomaji.com/instant.php?city=Tainan',
		'http://www.gomaji.com/instant.php?city=Kaohsiung']	

def CompleteURL(url):
	
	url = url[6:-1]
	'''
	if 'http://www.gohappy.com.tw' not in url:
		url =  'http://www.gohappy.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	if '&amp;' in url:
		url = url.replace('&amp;','&')
	if ' ' in url:
		url = url.replace(' ','')
	'''
	if '"' in url:
		url = url.split('"')[0]
	if '#' in url:
		return None
	return url
	
#先抓取第一頁的所有分類, 再來multi-thread
def GomajiPageInitial(url):
	'''
	page_urls = ['http://www.gomaji.com/Taipei',
		'http://www.gomaji.com/Taoyuan',
		'http://www.gomaji.com/Hsinchu',
		'http://www.gomaji.com/Miaoli',
		'http://www.gomaji.com/Taichung',
		'http://www.gomaji.com/Changhua',
		'http://www.gomaji.com/Nantou',
		'http://www.gomaji.com/Yunlin',
		'http://www.gomaji.com/Chiayi',
		'http://www.gomaji.com/Tainan',
		'http://www.gomaji.com/Kaohsiung',
		'http://www.gomaji.com/Pingtung',
		'http://www.gomaji.com/Taitung',
		'http://www.gomaji.com/Hualien',
		'http://www.gomaji.com/Yilan',
		'http://www.gomaji.com/Keelung',
		'http://www.gomaji.com/Taiwan',
		'http://www.gomaji.com/travel.php',
		'http://www.gomaji.com/instant.php?city=Taipei',
		'http://www.gomaji.com/instant.php?city=Taoyuan',
		'http://www.gomaji.com/instant.php?city=Hsinchu',
		'http://www.gomaji.com/instant.php?city=Taichung',
		'http://www.gomaji.com/instant.php?city=Tainan',
		'http://www.gomaji.com/instant.php?city=Kaohsiung']	
	'''
	'''
	for i in temp:
		url = i.split('href="')[1].split('"')[0]
		page_urls[Bucket.BucketHash(url,buk_num)].append(url)
	'''
	print 'Starting..'
def GomajiFetchPage(url):
	data = GetPageInfo.FetchWebPage(url)
	dic = []
	print url
	if '<li class="coupon-item">' in data:
		temp = data.split('<li class="coupon-item">')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				if 'http://www.gomaji.com' not in dic_format['url']:
					dic_format['url'] = 'http://www.gomaji.com/' + dic_format['url']
				dic_format['website'] = 'gomaji'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0]
				dic_format['date'] = time.strftime('%Y%m%d',time.localtime(time.time()))
				dic_format['title'] = i.split('</strike>')[1].split('</p>')[0]
				dic_format['price'] = i.split('<p>')[1].split('<span>')[1].split('元')[0]
				#print dic_format['title'], '\t', dic_format['price']
				dic.append(dic_format.copy())
			except:
				pass
				#print url
	elif '<div class="today-new-block radius shadow">' in data:
		temp = data.split('<div class="today-new-block radius shadow">')[1:]
		for i in temp:
			#try:
			dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':''}
			dic_format['url'] = i.split('href="')[1].split('"')[0]
			if 'http://www.gomaji.com' not in dic_format['url']:
				dic_format['url'] = 'http://www.gomaji.com/' + dic_format['url']
			dic_format['website'] = 'gomaji'
			dic_format['img_url'] = i.split('src="')[3].split('"')[0]
			dic_format['date'] = time.strftime('%Y%m%d',time.localtime(time.time()))
			dic_format['title'] = i.split('target="_blank">')[1].split('</a>')[0]
			dic_format['price'] = i.split('$')[1].split('<')[0]
			print dic_format['title'], '\t', dic_format['price']
			dic.append(dic_format.copy())
			#except:
			#	pass

	if dic != None:
		for i in dic:
			if i['url'] not in item_urls:
				item_urls.append(i['url'])
				item_temp.write(url+'\n')
			
				item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
				item_data.write(item)



class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
				
			GomajiFetchPage(url)
			self.queue.task_done()	


def GomajiPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(20):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)
	print page_urls
	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()

	url = ''
	GomajiPageInitial(url)
	GomajiPageSpider()

	#GoHappyItemSpider()
	f.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'