#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import PCHomeItemParser
import Parser

item_urls = []
item_temp = open('gohappy_item_temp','w')
item_data = open('gohappy_item_data','w')
page_urls = []

f = open('gohappy_page_temp','w')
f2 = open('gohappy_store_temp','w')

def CompleteURL(url):
	
	url = url[6:-1]
	if 'http://www.gohappy.com.tw' not in url:
		url =  'http://www.gohappy.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	if '&amp;' in url:
		url = url.replace('&amp;','&')
	if ' ' in url:
		url = url.replace(' ','')
	return url


#先抓取第一頁的所有分類, 再來multi-thread
def GoHappyPageInitial(url):
	data = GetPageInfo.FetchWebPage(url)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	for i in m1:
		url = CompleteURL(i)
		if 'event' in url or 'shopping' in  url:
			if 'javascript' not in url and '&pid=' not in url and 'css' not in url and 'facebook' not in url and 'plurk' not in url:
				if url not in page_urls:
					page_urls.append(url)

def GoHappyFetchPage(url):
	'''
	data = GetPageInfo.FetchWebPage(url)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []
	'''
	dic, data = Parser.GoHappyPageParser(url)
	if dic != None:
		for i in dic:
			if i['url'] not in item_urls:
				item_urls.append(i['url'])
				item_temp.write(url+'\n')
			
				item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
				item_data.write(item)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []

	for i in m1:
		url = CompleteURL(i)
		if 'event' in url or 'shopping' in  url:
			if 'javascript' not in url and '&pid=' not in url and 'css' not in url and 'facebook' not in url and 'plurk' not in url:
				if url not in page_urls:
					page_urls.append(url)
					do_url.append(url)
					f.write(url+'\n')

	for url in do_url:
		GoHappyFetchPage(url)


'''
def GoHappyFetchPage(url):

	data = GetPageInfo.FetchWebPage(url)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []

	for i in m1:
		url = CompleteURL(i)
		if 'event' in url or 'shopping' in  url:
			if 'javascript' not in url and '&pid=' not in url and 'css' not in url and 'facebook' not in url and 'plurk' not in url:
				if url not in page_urls:
					page_urls.append(url)
					do_url.append(url)
					f.write(url+'\n')

			elif '&pid=' in url:
				if url not in item_urls:
					item_urls.append(url)
					item_temp.write(url+'\n')

	for url in do_url:
		GoHappyFetchPage(url)

'''

class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
				
			GoHappyFetchPage(url)
			self.queue.task_done()	


def GoHappyPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(20):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()

	url = 'http://www.gohappy.com.tw/intro/sitemap.html'	

	GoHappyPageInitial(url)
	GoHappyPageSpider()
	#GoHappyStoreSpider(page_urls)

	#GoHappyItemSpider()
	f.close()
	f2.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'