#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
#import PCHomeItemParser
import Bucket
import Parser

buk_num = 100

item_urls = [] 
item_temp = open('etmall_item_temp','w',1)
item_data = open('etmall_item_data','w',1)
page_urls = Bucket.CreateBucket(buk_num) 

f = open('etmall_page_temp','w',1)
error_log = open('etmall_error','w',1)


def CompleteURL(url):
	
	url = url.split('"')[0]
	if '\\' in url:
		url = url.replace('\\','')
	'''
	if 'http://www.gohappy.com.tw' not in url:
		url =  'http://www.gohappy.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	if '&amp;' in url:
		url = url.replace('&amp;','&')
	if ' ' in url:
		url = url.replace(' ','')
	'''
	if 'http://www.etmall.com.tw' not in url:
		url =  'http://www.etmall.com.tw/' + url
	if '"' in url:
		url = url.split('"')[0]
	if '#' in url:
		return None
	return url
#先抓取第一頁的所有分類, 再來multi-thread
def ETMallPageInitial(url):
	data = GetPageInfo.FetchWebPage(url)

	m1 = data.split('URL": "')[1:]

	for i in m1:
		url = CompleteURL(i)
		if url != None and 'List' in url:	
			if Bucket.BucketSearch(url,page_urls,buk_num) == False:
				page_urls[Bucket.BucketHash(url,buk_num)].append(url)
				f.write(url+'\n')

def ETMallFetchPage(url):
	page_url = url[:]

	do_url = []
	#try:
	dic, data = Parser.ETMallPageParser(url)
	if dic != None:
		for i in dic:
			item_urls.append(i['url'])
			item_temp.write(url+'\n')
		
			item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
			item_data.write(item)

	#match = re.compile('href="[^\>]+"')
	#match = re.compile('URL": "[^\>]+"')
	#m1 = match.findall(data)
	m1 = data.split('URL": "')[1:]
	for i in m1:
		url = CompleteURL(i)
		if url != None:
			if 'List' in url:
				if Bucket.BucketSearch(url,page_urls,buk_num) == False:
					page_urls[Bucket.BucketHash(url,buk_num)].append(url)
					do_url.append(url)
					f.write(url+'\n')
	page = 1
	temp_dic = ''
	while True:
		temp_dic, data = Parser.ETMallPageParser(page_url+'&ProductPage='+str(page))
		if temp_dic != dic:
			if temp_dic != None:
				for i in temp_dic:
					item_urls.append(i['url'])
					item_temp.write(url+'\n')
				
					#item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
					item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
					item_data.write(item)
			try:
				dic = temp_dic[:]
			except:
				pass
			f.write(page_url+'&ProductPage='+str(page)+'\n')
			page += 1
		else:
			break

		if page > 50:
			break

		

	#except Exception,e:
	#	error_log.write(str(e)+'\n')
	for url in do_url:
		ETMallFetchPage(url)
'''
def RakutenFetchPage(url):
	d = url.split('d=')[1].split('&')[0]

	page = 1

	while True:
		url = 'http://search.rakuten.com.tw/?so=4&al=0&p=' + str(page) + '&d=' + d + '&vm=2' 
		try:
			dic, data = Parser.RakutenPageParser(url)
			if dic != None:
				for i in dic:
					item_urls.append(i['url'])
					item_temp.write(url+'\n')
				
					item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
					item_data.write(item)
			else:
				break

			page += 1

		except Exception,e:
			error_log.write(str(e)+'\n')

	
'''

class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
				
			ETMallFetchPage(url)
			self.queue.task_done()	


def ETMallPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(1):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for buk in page_urls:
		if len(buk) > 0:	
			for url in buk:
				queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()

	#此網頁要能夠看到所有大分類
	url = 'http://www.etmall.com.tw/sitemap.aspx'	

	ETMallPageInitial(url)
	ETMallPageSpider()

	#GoHappyItemSpider()
	f.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'