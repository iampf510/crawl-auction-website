#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys
import GetPageInfo
import time, urllib2

url = 'http://www.momoshop.com.tw/category/DgrpCategory.jsp?d_code=1116300088&mdiv=1116300000-bt_2_050_01&ctype=B&p_pageNum=42' 

def GetTime():
	return  str(int(time.time()))

def PCHomeItemParser(url):
	data = GetPageInfo.FetchWebPage(url)
	
	dic = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
	
	#商品網址
	dic['url'] = url

	#商品位於那個拍賣網站
	dic['website'] = 'pchome'

	#抓取此商品的日期
	#dic['date'] = time.strftime('%Y%m%d',time.localtime(time.time()))
	#dic['date'] = str(int(time.time()))
	dic['date'] = GetTime() 

	#抓商品照片
	data = data.split('<input type="hidden" id="')
	for i in data:
		if '<span id=\'spec_pic\'><img src="//' in i:
			i = i.split('<span id=\'spec_pic\'><img src="//')[1].split('" alt=')[0]
			dic['img_url'] = i
			
	data = data[1:-2]

	for i in data:
		#抓標題
		if 'name="IT_NAME' in i:
			#i = i.split('value=')[1][1:-3]
			i = i.split('name="IT_NAME')[1].split('value="')[1].split('"')[0]
			#i = unicode(i,'big5').encode('utf-8')
			try:
				i = i.decode('big5','ignore').encode('utf-8')
			except:
				i = i.replace(' ','')
				i = i.decode('big5').encode('utf-8')
				pass
			if '\t' in i:
				i = i.replace('\t','')
			dic['title'] = i
		#抓價格
		if 'name="IT_PRICE' in i:
			i = i.split('value=')[1][1:-3]
			print i
			dic['price'] = i
	
	return dic	

def YahooItemParser(url):
	data = GetPageInfo.FetchWebPage(url)
	
	dic = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
	
	#商品網址
	dic['url'] = url

	#商品位於那個拍賣網站
	dic['website'] = 'yahoo'

	#抓取此商品的日期
	dic['date'] = str(int(time.time()))

	#抓商品照片
	dic['img_url'] = data.split('<div class="Prod_img">')[1].split('<img src="')[1].split('" alt="')[0]

	#抓商品名稱
	dic['title']= data.split('Prod_Title')[1].split('<h1>')[1].split('</h1>')[0]

	#抓商品價格
	dic['price'] = data.split('<span id="TOTAL_Price_CARD" class="Price" itemprop="price">')[1].split('</span>')[0]

	return dic

def GoHappyItemParser(url):
	data = GetPageInfo.FetchWebPage(url)
	#data = urllib2.urlopen(url).read()
	dic = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}

	#商品網址
	dic['url'] = url
	
	#商品位於那個拍賣網站
	dic['website'] = 'gohappy'

	#抓取此商品的日期
	#dic['date'] = time.strftime('%Y%m%d',time.localtime(time.time()))
	#dic['date'] = str(int(time.time()))
	dic['date'] = GetTime()

	#抓商品照片
	dic['img_url'] = data.split('<img onmouseover=\'changeimg("bigimg","')[1].split('"')[0]
	print dic['img_url']

	#抓商品名稱
	dic['title'] = data.split('<title>GOHAPPY快樂購物網:')[1].split(',')[0]
	print dic['title']

	#抓商品價格
	try:
		dic['price'] = data.split('<span class="discount">')[1].split('</span>')[1].split('<span class')[0].rstrip()
	except:
		if '<dt>促銷價</dt>' in data:
			dic['price'] = data.split('<span class="moneyB">')[2].split('</span>')[0].split()[0]
		else:
			dic['price'] = data.split('<span class="moneyB">')[1].split('</span>')[0].split()[0]
	print dic['price']

def GoHappyPageParser(url):

        data = GetPageInfo.FetchWebPage(url)
        #dic = {}
        dic = []
        temp = data.split('<li class')
	try:
		catagory = data.split("GOHAPPY快樂購物網:")[1].split(",")[0]
	except:
		return None, data
        for i in temp:
                if 'price' in i and '點數' not in i:
			#catagory = data.split("GOHAPPY快樂購物網:")[1].split(",")[0]
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				if 'pid' not in dic_format['url']:
					break
				if 'www.gohappy.com.tw' not in dic_format['url']:
					dic_format['url'] =  'http://www.gohappy.com.tw' + dic_format['url']
				dic_format['website'] = 'gohappy'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0]
				if 'www.gohappy.com.tw' not in dic_format['img_url']:
					dic_format['img_url'] =  'http://www.gohappy.com.tw' + dic_format['img_url']
				#dic_format['date'] = time.strftime('%Y%m%d',time.localtime(time.time()))
				#dic_format['date'] = str(int(time.time()))
				dic_format['date'] = GetTime()
				dic_format['title'] = i.split('title="')[1].split('"')[0]
				dic_format['price'] = i.split('price">')[1].split('</span')[0]
				#dic_format['catagory'] = i.split('<li><a href=')[1].split('title=')[1].split('>')[0]
				dic_format['catagory'] = catagory
				dic.append(dic_format.copy())
				#print 'Here:',dic_format['catagory'] 
				#print url	
			except Exception,e:
				#print str(e)
				pass

	if len(dic) == 0:
		return None, data

        return dic, data

def YahooPageParser(url):
	
	data = GetPageInfo.FetchWebPage(url)
	if '?sub=' in url:
		return None, data

	#dic = {}
	dic = [] 
	temp = data.split('<ul class="pds')[1:]
	try:
		catagory = data.split('<li class="list subact"')[1].split('<a href=')[1].split('">')[1].split('</a>')[0]
	except:
		return None, data
	for i in temp:
		if 'price' in i and '點數' not in i:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				if 'buy.yahoo.com.tw' not in dic_format['url']:
					dic_format['url'] = 'http://buy.yahoo.com.tw' + dic_format['url']
				dic_format['website'] = 'yahoo'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0]
				dic_format['date'] = GetTime()
				dic_format['title'] = i.split('alt="')[1].split('"')[0]
				dic_format['price'] = i.split('<span class="nums">')[1].split('>')[1].split('<')[0]
				dic_format['catagory'] = catagory
				#print dic_format['title'], '\t', dic_format['price']
				dic.append(dic_format.copy())
			except:
				pass
				#print url
	if len(dic) == 0:
		return None, data

	return dic, data	

def RakutenPageParser(url):
	
	data = GetPageInfo.FetchWebPage(url)
	dic = []
	
	if '<p class="itemPhoto">' in data:
		catagory = data.split('<strong class="current">')[1].split('</strong>')[0]
		temp = data.split('<p class="itemPhoto">')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				dic_format['website'] = 'rakuten'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0]
				dic_format['date'] = GetTime()
				dic_format['title'] = i.split('title="')[1].split('"')[0]
				dic_format['price'] = i.split('<li class="price">')[1].split('<span>')[1].split('</span>')[0]
				dic_format['catagory'] = catagory 
				#print dic_format['title'], '\t', dic_format['img_url']
				dic.append(dic_format.copy())
			except Exception,e :
				print str(e)
				pass
				#print url
	elif '<td class="img">' in data:
		catagory = data.split('<strong class="current">')[1].split('</strong>')[0]
		temp = data.split('<td class="img">')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				dic_format['website'] = 'rakuten'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0]
				dic_format['date'] = GetTime()
				dic_format['title'] = i.split('title="')[1].split('"')[0]
				dic_format['price'] = i.split('<li class="price">')[1].split('<span>')[1].split('</span>')[0]
				dic_format['catagory'] = catagory
				#print dic_format['title'], '\t', dic_format['img_url']
				dic.append(dic_format.copy())
			except:
				pass
				#print url
	else:
		return None, data

	if len(dic) == 0:
		print 'hello'
		return None, data

	return dic, data	

def MoMoPageParser(url, proxy):

	data = GetPageInfo.FetchWebPage(url,proxy)
	if data == '':
		return None, None
	if '<div class="Dgrp_img">' not in data and 'class="PageBody">' not in data:
		return None, data

	data = data.decode('big5','ignore').encode('utf-8')
	dic = []
	if '<div class="Dgrp_img">' in data:	
		catagory = data.split('<li cateCode="')[1].split('">')[2].split('</a>')[0]
		temp = data.split('<div class="Dgrp_img">')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = 'http://www.momoshop.com.tw' + i.split('href="')[1].split('"')[0]
				if 'http://www.momoshop.com.tw' not in dic_format['url']:
					dic_format['url'] = 'http://www.momoshop.com.tw' + dic_format['url']
				dic_format['website'] = 'momo'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0]
				if 'http://www.momoshop.com.tw' not in dic_format['img_url']:
					dic_format['img_url'] = 'http://www.momoshop.com.tw' + dic_format['img_url']
				dic_format['date'] = GetTime()
				dic_format['title'] = i.split('title="')[1].split('"')[0]
				dic_format['price'] = i.split('<span class="Dgrp_redbp1">')[1].split('</span>')[0]
				dic_format['catagory'] = catagory
				#print dic_format['title'], '\t', dic_format['url']
				dic.append(dic_format.copy())
			except:
				pass
				#print url

	elif 'class="PageBody">' in data:
		catagory = data.split('<li cateCode="')[1].split('">')[2].split('</a>')[0]
		temp = data.split('class="PageBody">')[1:]
		for i in temp:
			#try:
			dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
			dic_format['url'] = url 
			dic_format['website'] = 'momo'
			dic_format['img_url'] = i.split('src="')[1].split('"')[0]
			if 'http://www.momoshop.com.tw' not in dic_format['img_url']:
				dic_format['img_url'] = 'http://www.momoshop.com.tw' + dic_format['img_url']
			dic_format['date'] = GetTime()
			dic_format['title'] = i.split('title="')[1].split('"')[0]
			dic_format['price'] = i.split('class="pinkWord18">')[1].split('</span>')[0]
			dic_format['catagory'] = catagory
			#print dic_format['title'], '\t', dic_format['price']
			dic.append(dic_format.copy())
			#except:
			#	pass
			#	#print url

	if len(dic) == 0:
		return None, data
	
	return dic, data	


def ETMallPageParser(url):

        data = GetPageInfo.FetchWebPage(url)
        dic = []

        if 'aPL_GOODID[' in data:
		try:
			catagory = data.split('<ul id="TopicPath">')[1].split('</a></li></ul>')[0].split('">')[-1]
		except:
			return None, data
                temp = data.split('aPL_GOODID[')[1:]
                for i in temp:
                        try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
                                goodid = i.split('"')[1]
                                dic_format['url'] = 'http://www.etmall.com.tw/Pages/ProductDetail.aspx?ProductSKU=' + goodid
                                dic_format['website'] = 'etmall'
                                dic_format['img_url'] = 'http://media.etmall.com.tw/ProductImage/'+goodid+'/'+goodid+'_M.jpg'
                                dic_format['date'] = time.strftime('%Y%m%d',time.localtime(time.time()))
                                dic_format['title'] = i.split('\'')[1]
                                dic_format['price'] = i.split('aPL_PRC[')[1].split('"')[1].split('"')[0]
				dic_format['catagory'] = catagory
                                dic.append(dic_format.copy())

                                #print dic_format['title'], '\t', dic_format['price']
                        except:
                                print 'error'
                                pass
                                #print url
        else:
                return None, data

        if len(dic) == 0:
                return None, data

        return dic, data
'''
def ETMallPageParser(url):
	
	data = GetPageInfo.FetchWebPage(url)
	dic = []

	if 'aPL_GOODID[' in data:
		catagory = data.split('<ul id="TopicPath">')[1].split('</a></li></ul>')[0].split('">')[-1]
		temp = data.split('aPL_GOODID[')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				goodid = i.split('"')[1]
				dic_format['url'] = 'http://www.etmall.com.tw/Pages/ProductDetail.aspx?ProductSKU=' + goodid
				dic_format['website'] = 'etmall'
				dic_format['img_url'] = 'http://media.etmall.com.tw/ProductImage/'+goodid+'/'+goodid+'_M.jpg'
				dic_format['date'] = GetTime()
				dic_format['title'] = i.split('\'')[1]
				dic_format['price'] = i.split('aPL_PRC[')[1].split('"')[1].split('"')[0]
				dic_format['catagory'] = catagory
				dic.append(dic_format.copy())

				#print dic_format['title'], '\t', dic_format['price']
			except:
				print 'error'
				pass
				#print url
	else:
		return None, data

	if len(dic) == 0:
		return None, data

	return dic, data	
'''
def GroupOnPageParser(url):
	
	data = GetPageInfo.FetchWebPage(url)
	dic = []

	if '<div style="width:1px; height:10px; overflow:hidden;"></div>' in data:
		temp = data.split('<div style="width:1px; height:10px; overflow:hidden;"></div>')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				if 'http://www.groupon.com.tw' not in dic_format['url']:
					dic_format['url'] = 'http://www.groupon.com.tw' + dic_format['url'] 
				dic_format['website'] = 'groupon'
				dic_format['img_url'] = i.split('src="')[1].split('"')[0] 
				dic_format['date'] = str(int(time.time()))
				dic_format['title'] = i.split('title=')[1].split('>')[1].split('<')[0]
				dic_format['price'] = i.split('團購價')[1].split('">')[2].split('<')[0]
				dic.append(dic_format.copy())
				#print dic_format['title'], '\t', dic_format['url']
			except:
				print 'error'
				pass
				#print url

	else:
		return None, data

	if '<div class="deallist2_city_top"></div>' in data:
		temp = data.split('<div class="deallist2_city_top"></div>')[1:]
		for i in temp:
			try:
				dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
				dic_format['url'] = i.split('href="')[1].split('"')[0]
				if 'http://www.groupon.com.tw' not in dic_format['url']:
					dic_format['url'] = 'http://www.groupon.com.tw' + dic_format['url'] 
				dic_format['website'] = 'groupon'
				dic_format['img_url'] = i.split('rel="')[1].split('"')[0] 
				dic_format['date'] = str(int(time.time()))
				dic_format['title'] = i.split('title=')[1].split('>')[1].split('<')[0]
				dic_format['price'] = i.split('團購價')[1].split('">')[1].split('<')[0]
				dic.append(dic_format.copy())
				#print dic_format['title'], '\t', dic_format['img_url']
			except:
				print 'error'
				pass
				#print url

	else:
		return None, data

	if len(dic) == 0:
		return None, data

	return dic, data	

def PayeasyPageParser(url):

        data = GetPageInfo.FetchWebPage(url)

        #dic = {}
        dic = []
        temp = data.split('<div class="l2_nine_gr_mia')[2:]
	catagory = data.split('您現在的位置:')[1].split('</a></label>')[0].split(' > ')[1]
	print 'here', catagory
        print len(temp)
        for i in temp:
                try:
			dic_format = {'title':'', 'url':'', 'price':'', 'img_url':'', 'website':'', 'date':'', 'catagory':''}
                        dic_format['url'] = i.split('href="')[1].split('"')[0]
                        if '&amp;' in dic_format['url']:
                                dic_format['url'] = dic_format['url'].replace('&amp;','&')
                        dic_format['website'] = 'payeasy'
                        dic_format['img_url'] = i.split('src="')[1].split('"')[0]
			dic_format['date'] = GetTime()
                        dic_format['title'] = i.split('title="')[1].split('"')[0]
                        dic_format['price'] = i.split('<span class="nine_gr_pricen">')[1].split('<')[0]
			dic_format['catagory'] = catagory
                        #print dic_format['title'], '\t', dic_format['url']
                        dic.append(dic_format.copy())
                except:
                        #print i
                        pass
                        #print url
        if len(dic) == 0:
                return None, data

        return dic, data

if __name__ == '__main__':
	#url = 'http://search.rakuten.com.tw/blouse/' 
	#GoHappyPageParser(url)
	#dic, data = RakutenPageParser(url)
	#dic, data = MoMoPageParser(url, '202.95.141.129:8080')
	MoMoPageParser(url, None)