#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import Parser

item_urls = []
item_temp = open('pchome_item_temp','w')
item_data = open('pchome_item_data','w')
error_log = open('pchome_error_log','w')

#將連結補成完整URL
def CompleteURL(url):	
	url = url[6:-1]
	if 'shopping.pchome.com.tw' not in url:
		url = 'http://shopping.pchome.com.tw/' + url
	return url

def PCHomePageSpider(url, urls, page_urls, f, f2):
	urls.append(url)
	data = GetPageInfo.FetchWebPage(url)
	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)
	for i in m1:
		#抓分類頁面
		if 'sitemap' in i and 'javascript' not in i:
			url = CompleteURL(i)
			if url not in urls:
				f.write(url)
				f.write('\n')
				PCHomePageSpider(url,urls,page_urls,f,f2)
		#抓分頁頁面
		if 'store' in i:
			url = CompleteURL(i)
			if url not in page_urls:
				page_urls.append(url)
				f2.write(url+'\n')
				
	return 

class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		noise_pattern = ['Remote','unlink','缺少','沒有','不','錯','hotitem','area','stor','activity','pick','css','js','gif','GoTo','ico','sitemap','view','special','0x','ec1img','bonus','search']	#垃圾訊息, 刪除
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
			data = GetPageInfo.FetchWebPage(url)
			match = re.compile('href="[^\>]+"')
			m1 = match.findall(data)
			
			#抓取商品頁面URL
			for i in m1:
				if '-' in i and 'ec1img' not in i:
					check_noise = False
					for pattern in noise_pattern:
						if pattern in i:
							check_noise = True
					if check_noise == False:
						url = CompleteURL(i)
						if '"' in url:
							url = url.split('"')[0]
						if url not in item_urls:
							item_urls.append(url)
							#print url

				# 如果還有分頁
				if 'store' in i and 'page=' in i and 'SR_NO=BMAA0P' not in i:
					url = CompleteURL(i)
					if url not in page_urls:
						page_urls.append(url)
						data = GetPageInfo.FetchWebPage(url)
						m2 = match.findall(data)
						for j in m2:
							if '-' in j and 'ec1img' not in j:
								check_noise = False
								for pattern in noise_pattern:
									if pattern in j:
										check_noise = True
								if check_noise == False:
									url = CompleteURL(j)
									if '"' in url:
										url = url.split('"')[0]
									if url not in item_urls:
										item_temp.write(url+'\n')
										item_urls.append(url)
			
			self.queue.task_done()	


#抓分頁頁面
def PCHomeStoreSpider( page_urls ):
	queue = Queue.Queue()
	threads = []

	for i in range(20):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()

#抓商品頁面
class ItemWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
			try:
				#item_temp.write(url+'\n')
				dic = Parser.PCHomeItemParser(url)
				if dic != None:
					data = dic['title'] + '\t' + dic['url'] + '\t' + dic['price'] + '\t' + dic['img_url'] + '\t' + dic['website'] + '\t' + dic['date'] + '\n'
					item_data.write(data)
					'''
					item_data.write(dic['title'])	
					item_data.write('\t')
					item_data.write(dic['url'])
					item_data.write('\t')
					item_data.write(dic['price'])
					item_data.write('\t')
					item_data.write(dic['img_url'])
					item_data.write('\t')
					item_data.write(dic['website'])
					item_data.write('\t')
					item_data.write(dic['date'])
					item_data.write('\n')
					'''
			except:
				error_log(url+'\n')

			self.queue.task_done()	

#抓商品內容
def PCHomeItemSpider( ):
	queue = Queue.Queue()
	threads = []

	for i in range(20):
		worker = ItemWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in item_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()

if __name__ == '__main__':
	start = time.time()
	urls = []	#之後要抓的商品URL
	page_urls = []

	url = 'http://shopping.pchome.com.tw/?mod=sitemap'	
	f = open('pchome_page_temp','w')
	f2 = open('pchome_store_temp','w')

	PCHomePageSpider(url, urls, page_urls, f, f2)
	PCHomeStoreSpider(page_urls)

	x = open('pchome_item_page_temp','w')
	for i in item_urls:
		x.write(i+'\n')
	x.close()

	PCHomeItemSpider()

	f.close()
	f2.close()
	
	item_data.close()
	item_temp.close()
	end = time.time()
	
	print end-start ,'sec'