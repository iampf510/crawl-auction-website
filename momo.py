#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import PCHomeItemParser
import Parser
import random
import GlobalVar

item_urls = []
item_temp = open('momo_temp','w',1)
item_data = open('momo_item_data','w',1)
page_urls = []

proxy_list = Queue.Queue()
f = open('momo_page_temp','w')
error_log = open('momo_error_log','w',1)


total_item = 0
max_item = 600000 
def GetPageByProxy(url, proxy):
	try_time = 0
	if proxy != None:
		proxy_list.put(proxy)
		proxy = proxy_list.get(timeout=1)
	else:
		time.sleep(random.randint(1,3))
		#time.sleep(0.5)
	while True:
		#dic, data = Parser.MoMoPageParser(url,proxy)
		if 'http' in url:
			url = 'http' + url.split('http')[1]
		dic, data = Parser.MoMoPageParser(url,proxy)
		if dic == None and data == None:
			try_time += 1
			if proxy == None:
				data = '123'
				break
			if try_time >= 10:
				break
			if proxy != None:
				proxy_list.put(proxy)
				proxy = proxy_list.get(timeout=1)
		elif '<a href="https://www.momoshop.com.tw/mypage/MemberCenter.jsp?func=40&ctype=B&cid=downline&oid=012&mdiv=1000200000-bt_0_042_01&ctype=B" style="text-decoration:underline">' not in data:
			try_time += 1
			if try_time >= 10:
				break
			if proxy != None:
				proxy_list.put(proxy)
				proxy = proxy_list.get(timeout=1)
		else:
			if proxy != None:
				proxy_list.put(proxy)
				proxy = proxy_list.get(timeout=1)
			#print url, proxy
			break
	try:
		data = data.decode('big5','ignore').encode('utf-8')
	except:
		data = '123'
	return dic, data, proxy	

def GetProxyList():
	#f2 = open('very_ok_proxy','r')	
	f2 = open('ok_proxy','r')	
	#f2 = open(GlobalVar.PROXY_FILE,'r')	
	#proxy_list.put(None)			#放入本機
	while True:
		line = f2.readline()
		if line:
			line = line.rstrip()
			proxy_list.put(line)	#放入proxy
		else:
			break

def CompleteURL(url):
	
	url = url[6:-1]
	if 'http://www.momoshop.com.tw' not in url:
		url = 'http://www.momoshop.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	return url

#先抓取第一頁的所有分類, 再來multi-thread
def MoMoPageInitial(url):

	data = GetPageInfo.FetchWebPage(url)
	time.sleep(3)
	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)
	for i in m1:
		url = CompleteURL(i)
		if '/category/' in url:
			if url not in page_urls:
				#print url
				page_urls.append(url)


def MoMoFetchPage(url,proxy):
	check_total = False
	global total_item
	
	dic, data, proxy = GetPageByProxy(url,proxy)
	#time.sleep(random.randint(2,5))
	if total_item < max_item:
		if dic != None:
			for i in dic:
				if i['url'] not in item_urls:
					total_item += 1
					item_urls.append(i['url'])
					
					if ',' in i['price']:
						i['price'] =  i['price'].replace(',','')
					item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
					item_data.write(item)
			item_temp.write(url+'\n')
			if total_item >= max_item:
				check_total = True
			page = 2
			while True:
				temp_url = url + '&p_pageNum=' + str(page)
				#dic, data = Parser.MoMoPageParser(temp_url)
				dic, data, proxy = GetPageByProxy(temp_url,proxy)
				if '目前暫無資料' in data or '沒有符合的資料' in data :
					break
				#time.sleep(random.randint(2,5))
				if dic != None:
					for i in dic:
						if i['url'] not in item_urls:
							total_item += 1
							item_urls.append(i['url'])
							item_temp.write(url+'\n')
							if ',' in i['price']:
								i['price'] =  i['price'].replace(',','')
			
							item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
							#item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
							item_data.write(item)
							if total_item >= max_item:
								check_total = True
				else:
					break

				if check_total == True:
					break

				page += 1
				if page >= 50:
					break	
	
	if check_total == False:
		match = re.compile('href="[^\>]+"')
		m1 = match.findall(data)

		do_url = []

		for i in m1:
			url = CompleteURL(i)
			if '/category/' in url:
				if url not in page_urls:
					page_urls.append(url)
					do_url.append(url)
					f.write(url+'\n')

		for url in do_url:
			if proxy != None:
				proxy_list.put(proxy)
				proxy = proxy_list.get(timeout=1)
			if check_total == False:
				MoMoFetchPage(url, proxy)
		if proxy != None:
			proxy_list.put(proxy)

class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			global total_item
			if total_item >= max_item:
				self.queue.task_done()
				return
			else:
				try:
					#從queue中抽取出下一條要處理的URL
					url = self.queue.get(timeout=1)
				except Queue.Empty:
					#如果queue空了就跳出
					return

				if self.tid == 0:
					proxy = None
				else:
					proxy = proxy_list.get(timeout=1)	
				MoMoFetchPage(url,proxy)
				self.queue.task_done()	


def MoMoPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(50):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()
	GetProxyList()
	url = 'http://www.momoshop.com.tw/main/Main.jsp'	
	#url = 'http://www.momoshop.com.tw/category/DgrpCategory.jsp?d_code=2400600019'
	MoMoPageInitial(url)
	try:
		MoMoPageSpider()
	except Exception,e :
		print 'Error : ', str(e)
	#GoHappyStoreSpider(page_urls)

	#GoHappyItemSpider()
	f.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'