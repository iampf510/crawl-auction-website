#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import temp_Parser
import Bucket

item_urls = [] 
item_temp = open('groupon_item_temp','w',1)
item_data = open('groupon_item_data','w',1)
page_urls = [] 

error_log = open('groupon_error','w',1)

def CompleteURL(url):
	
	url = url[6:-1]
	if '"' in url:
		url = url.split('"')[0]
	if '#' in url:
		return None
	return url
	
def GroupOnFetchPage(url):
	dic, data = temp_Parser.GroupOnPageParser(url) 
	if dic != None:
		for i in dic:
			item_urls.append(i['url'])
			item_temp.write(url+'\n')
		
			item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
			item_data.write(item)

if __name__ == '__main__':
	start = time.time()

	url = 'http://www.groupon.com.tw/deallist2.php'
	GroupOnFetchPage(url)
	item_temp.close()
	item_data.close()
	end = time.time()
	
	print end-start ,'sec'