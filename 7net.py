#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import PCHomeItemParser
import Parser
import random
import GlobalVar

import t_Parser
item_urls = []
item_temp = open('7net_item_temp','w',1)
item_data = open('7net_item_data','w',1)
page_urls = []
urls = []
proxy_list = Queue.Queue()
f = open('7net_page_temp','w')
error_log = open('7net_error_log','w',1)

#ok_proxy = open('very_ok_proxy','w',1)
ok_proxy_list = []
def GetPageByProxy(url, proxy):
	try_time = 0
	if proxy != None:
		proxy_list.put(proxy)
		proxy = proxy_list.get(timeout=1)
	else:
		time.sleep(random.randint(3,5))

	while True:
		dic, data = t_Parser.SevenNetPageParser(url,proxy)
		if dic == None and data == None:
			try_time += 1
			if proxy == None:
				data = None 
				time.sleep(random.randint(3,5))
				break
			if try_time >= 10:
				data = None
				break
			proxy_list.put(proxy)
			proxy = proxy_list.get(timeout=1)
		elif '<li class="first"><span>|</span><a href="http://www.7net.com.tw/knowledge/knowledgeA1.html">' not in data:
			try_time += 1
			if try_time >= 10:
				data = None
				break
			
			proxy_list.put(proxy)
			proxy = proxy_list.get(timeout=1)

		else:
			print url, proxy
			break
	if proxy not in ok_proxy_list and proxy != None and data != None:
		ok_proxy_list.append(proxy)
		#ok_proxy.write(proxy+'\n')
	try:
		data = data.decode('big5','ignore').encode('utf-8')
	except:
		data = '123'
	return dic, data, proxy	

def GetProxyList():
	#f2 = open(GlobalVar.PROXY_FILE,'r')	
	f2 = open('ok_proxy','r')	
	#f2 = open('tt_proxy','r')	
	#proxy_list.put(None)			#放入本機
	while True:
		line = f2.readline()
		if line:
			line = line.rstrip()
			proxy_list.put(line)	#放入proxy
		else:
			break

def CompleteURL(url):
	
	url = url[6:-1]
	if 'http://www.7net.com.tw' not in url:
		url = 'http://www.7net.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	return url

def SevenNetFetchAllPage(url,proxy):
	time.sleep(random.randint(4,7))
	while True:
		data = GetPageInfo.FetchWebPage(url,proxy)
		if data == '':
			if proxy != None:
				proxy_list.put(proxy)
				proxy = proxy_list.get(timeout=1)
		else:
			break
	temp = data.split('<h3  class="open ">')
	for i in temp:
		x = i.split('href="')[1].split('"')[0]
		if 'http://www.7net.com.tw/7net/rui00' in x and x not in page_urls:
			page_urls.append(x)
			f.write(x+'\n')


class AllPageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		if self.tid == 0:
			proxy = None
		else:
			proxy = proxy_list.get(timeout=1)	
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
			SevenNetFetchAllPage(url,proxy)
			self.queue.task_done()	


def SevenNetGetAllPage():
	queue = Queue.Queue()
	threads = []

	for i in range(50):
		worker = AllPageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	



#先抓取第一頁的所有分類, 再來multi-thread
def SevenNetPageInitial(url):

	data = GetPageInfo.FetchWebPage(url)
	time.sleep(3)

	temp = data.split('<div class="SAD01">')[1].split('</div></li><li class="MARKETING">')[0]
	temp = temp.split('<li><a href="')[1:]

	for i in temp:
		x = i.split('"')[0]
		if 'faces?' in x:
			urls.append(i.split('"')[0])

	SevenNetGetAllPage()


def SevenNetFetchPage(url,proxy,queue):

	#time.sleep(random.randint(4,7))
	dic, data, proxy = GetPageByProxy(url,proxy)
	if dic != None:
		for i in dic:
			if i['url'] not in item_urls:
				item_urls.append(i['url'])
				item_temp.write(url+'\n')
			
				item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
				item_data.write(item)
		
		if 'rui003' in url:
			if 'pageNo' not in url:
				url += '&stop=1&pageNo=2'
				if url not in page_urls:
					page_urls.append(url)
					queue.put(url)
			else:
				page = str(int(url.split('pageNo=')[1])+1)
				url = url.split('pageNo=')[0]
				url += 'pageNo=' + page
				if url not in page_urls:
					page_urls.append(url)
					queue.put(url)
	
	'''
	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []
	for i in m1:
		url = CompleteURL(i)
		if '/category/' in url:
			if url not in page_urls:
				page_urls.append(url)
				do_url.append(url)
				f.write(url+'\n')
	'''
	'''
	for url in do_url:
		SevenNetFetchPage(url, proxy)
	'''
	if proxy != None:
		proxy_list.put(proxy)


class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
			if self.tid == 1:
				proxy = None
			else:
				proxy = proxy_list.get(timeout=1)
			SevenNetFetchPage(url,proxy,self.queue)
			self.queue.task_done()	


def SevenNetPageSpider():
	GetProxyList()
	queue = Queue.Queue()
	threads = []

	for i in range(50):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()
	GetProxyList()
	url = 'http://www.7net.com.tw/7net/rui001.faces'	
	#url = 'http://www.momoshop.com.tw/category/DgrpCategory.jsp?d_code=2400600019'
	SevenNetPageInitial(url)
	SevenNetPageSpider()

	f.close()
	item_temp.close()
	item_data.close()
	end = time.time()
	
	print end-start ,'sec'