#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
import PCHomeItemParser
import Parser

item_urls = []
item_temp = open('yahoo_item_temp','w')
item_data = open('yahoo_item_data','w')
page_urls = []

f = open('yahoo_page_temp','w')
f2 = open('yahoo_store_temp','w')

def CompleteURL(url):
	
	url = url[6:-1]
	if 'http://buy.yahoo.com.tw' not in url:
		url =  'http://buy.yahoo.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	if '&amp;' in url:
		url = url.replace('&amp;','&')
	if ' ' in url:
		url = url.replace(' ','')
	return url


#先抓取第一頁的所有分類, 再來multi-thread
def YahooPageInitial(url):
	data = GetPageInfo.FetchWebPage(url)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	for i in m1:
		#抓分類頁面
		if '?sub=' in i and 'search' not in i and 'jpg' not in i and 'data-dualcode' not in i and 'hpp=' not in i:
			url = CompleteURL(i)
			if 'style="color' in url:
				url = url[:-23]
			if 'data-ylk=' in url:
				url = url[:-21]
			if url not in page_urls:
				page_urls.append(url)
		if '?cati' in i :
			url = CompleteURL(i)
			if '"' in url:
				url = url.split('"')[0]
			if 'style="color' in url:
				url = url[:-23]
			if '?catitemid=65032&page=8&order=0' not in url:
				if url not in page_urls:
					page_urls.append(url)


def YahooFetchPage(url):
	'''
	data = GetPageInfo.FetchWebPage(url)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []
	'''

	dic, data = Parser.YahooPageParser(url)
	if dic != None:
		for i in dic:
			if i['url'] not in item_urls:
				item_urls.append(i['url'])
				item_temp.write(url+'\n')
			
				item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] +'<SPACE>' +i['catagory']+'\n'
				item_data.write(item)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	do_url = []

	for i in m1:
		url = CompleteURL(i)
		if '?sub=' in url:
			if 'search' not in url and 'jpg' not in url and 'data-dualcode' not in url and 'hpp=' not in url:
				if url not in page_urls:
					page_urls.append(url)
					do_url.append(url)
					f.write(url+'\n')

		if '?cati' in i :
			url = CompleteURL(i)
			if '"' in url:
				url = url.split('"')[0]
			if 'style="color' in url:
				url = url[:-23]
			if '?catitemid=65032&page=8&order=0' not in url:
				if url not in page_urls:
					do_url.append(url)
					page_urls.append(url)
					f.write(url+'\n')



	for url in do_url:
		YahooFetchPage(url)

class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
				
			YahooFetchPage(url)
			self.queue.task_done()	


def YahooPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(20):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)

	for url in page_urls:
		queue.put(url)

	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()

	url = 'http://buy.yahoo.com.tw/help/helper.asp?p=sitemap'	

	YahooPageInitial(url)
	YahooPageSpider()

	f.close()
	f2.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'