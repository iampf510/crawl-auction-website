#!/usr/bin/env python2
#! coding:utf-8
#! coding:big5
import sys,os
import Queue, threading
import urllib2
import pycurl
import re,time
import StringIO
import GetPageInfo
#import PCHomeItemParser
import Parser
import Bucket

buk_num = 100

item_urls = [] 
item_temp = open('rakuten_item_temp','w',1)
item_data = open('rakuten_item_data','w',1)
#page_urls = Bucket.CreateBucket(buk_num) 
page_urls = [] 

f = open('rakuten_page_temp','w',1)
error_log = open('rakuten_error','w',1)


def CompleteURL(url):
	
	url = url[6:-1]
	'''
	if 'http://www.gohappy.com.tw' not in url:
		url =  'http://www.gohappy.com.tw' + url
	if '"' in url:
		url = url.split('"')[0]
	if '&amp;' in url:
		url = url.replace('&amp;','&')
	if ' ' in url:
		url = url.replace(' ','')
	'''
	if '"' in url:
		url = url.split('"')[0]
	if '#' in url:
		return None
	return url
'''
#先抓取第一頁的所有分類, 再來multi-thread
def RakutenPageInitial(url):
	data = GetPageInfo.FetchWebPage(url)

	match = re.compile('href="[^\>]+"')
	m1 = match.findall(data)

	for i in m1:
		url = CompleteURL(i)
		if url is not None and url not in page_urls:
			if 'www.rakuten.com.tw' in url or 'search.rakuten.com.tw' in url:	
				page_urls.append(url)
'''
'''
#先抓取第一頁的所有分類, 再來multi-thread
def RakutenPageInitial(url):
	data = GetPageInfo.FetchWebPage(url)

	temp = data.split('<div class="shop-link-title">')[1:]
	for x in temp:
		url = x.split('href="')[1].split('"')[0]

		data = GetPageInfo.FetchWebPage(url)
		if '<p class="itemPhoto">' not in data or '<td class="img">' not in data:
			match = re.compile('href="[^\>]+"')
			m1 = match.findall(data)
			for i in m1:
				url = CompleteURL(i)
				if url == None:
					pass
				elif 'p=1&' in url and 'search.rakuten.com.tw' in url:
					print url

'''	
		
#先抓取第一頁的所有分類, 再來multi-thread
def RakutenPageInitial(url):
	data = GetPageInfo.FetchWebPage(url)
	#temp = data.split('<img src="http://c.tw.rakuten-static.com/front/common/img/t.gif" alt="" width="13" height="13" class="circle">')[1:]
	temp = data.split('<span class="search-sprite-icon-bullet listLink"></span>')[1:]
	for i in temp:
		url = i.split('href="')[1].split('"')[0]
		page_urls.append(url)

'''
def RakutenFetchPage(url):
	do_url = []
	try:
		dic, data = Parser.RakutenPageParser(url)
		if dic != None:
			for i in dic:
				item_urls.append(i['url'])
				item_temp.write(url+'\n')
			
				item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
				item_data.write(item)

		match = re.compile('href="[^\>]+"')
		m1 = match.findall(data)


		for i in m1:
			url = CompleteURL(i)
			if url != None:
				if 'search.rakuten.com.tw' in url:
					if 'vm=2' in url and '?so=4' in url and 'p=1&' not in url:
						if Bucket.BucketSearch(url,page_urls,buk_num) == False:
							page_urls[Bucket.BucketHash(url,buk_num)].append(url)
							do_url.append(url)
							f.write(url+'\n')
	except Exception,e:
		error_log.write(str(e)+'\n')
	for url in do_url:
		RakutenFetchPage(url)
'''
def RakutenFetchPage(url):
	d = url.split('d=')[1].split('&')[0]

	page = 1

	while True:
		url = 'http://search.rakuten.com.tw/?so=4&al=0&p=' + str(page) + '&d=' + d + '&vm=2' 
		try:
			dic, data = Parser.RakutenPageParser(url)
			print dic 
			if dic != None:
				for i in dic:
					item_urls.append(i['url'])
					item_temp.write(url+'\n')
				
					#item = i['title'] + '\t' + i['url'] + '\t' + i['price'] + '\t' + i['img_url'] + '\t' + i['website'] + '\t' + i['date'] + '\n'
					item = i['title'] + '<SPACE>' + i['url'] + '<SPACE>' + i['price'] + '<SPACE>' + i['img_url'] + '<SPACE>' + i['website'] + '<SPACE>' + i['date'] + '<SPACE>' + i['catagory'] + '\n'
					item_data.write(item)
			else:
				break

			page += 1

		except Exception,e:
			error_log.write(str(e)+'\n')



class PageWorker(threading.Thread):
	def __init__(self, queue, i):
		threading.Thread.__init__(self)
		self.queue = queue
		self.tid = i
	
	def run(self):
		while True:
			try:
				#從queue中抽取出下一條要處理的URL
				url = self.queue.get(timeout=1)
			except Queue.Empty:
				#如果queue空了就跳出
				return
				
			RakutenFetchPage(url)
			self.queue.task_done()	


def RakutenPageSpider():
	queue = Queue.Queue()
	threads = []

	for i in range(20):
		worker = PageWorker(queue,i)
		worker.setDaemon(True)
		worker.start()
		threads.append(worker)
	print page_urls
	for i in page_urls:
		queue.put(i)
	'''
	for buk in page_urls:
		if len(buk) > 0:	
			for url in buk:
				queue.put(url)
	'''
	queue.join()

	for item in threads:
		item.join()	


if __name__ == '__main__':
	start = time.time()

	#此網頁要能夠看到所有大分類
	url = 'http://search.rakuten.com.tw/jewel/'	

	RakutenPageInitial(url)
	RakutenPageSpider()

	#GoHappyItemSpider()
	f.close()
	item_temp.close()
	item_data.close()
	#page_temp.close()
	end = time.time()
	
	print end-start ,'sec'