#!/usr/bin/env python2
#! coding:utf-8

# 為了使搜尋更加快速, 寫了一個類似bucket search的東西, 可以將list分成n份
# 可以大幅加快速度
import md5

def CreateBucket(num):
	bucket = []
	for i in range(num):
		bucket.append([])

	return bucket

def BucketHash(url, bucket_num):
	url = md5.new(url).hexdigest()
	num = ord(url[-1]) * 2
	num = ord(url[-2]) * 3
	num = ord(url[-3]) * 4
	num = ord(url[-4]) * 5
	num = ord(url[-5]) * 6
	num = ord(url[-6]) * 7
	num = ord(url[-7]) * 8
	return num % bucket_num

def BucketSearch(url, bucket, bucket_num):
	num = BucketHash(url, bucket_num)
	if url in bucket[num]:
		return True
	else:
		return False

'''
#建一個新的bucket, num為bucket數
def CreateBucket(num):
	bucket = []
	#建出num個bucket出來
	for i in range(num):
		bucket.append([])
	
	return bucket

#找出url應該要放在第幾個bucket中
def BucketHash(url, bucket_num):
	num  = ord(url[-1]) * 2
        num += ord(url[-2]) * 3
        num += ord(url[-3]) * 4
        num += ord(url[-4]) * 5
        num += ord(url[-5]) * 6
        num += ord(url[-6]) * 7
        num += ord(url[-7]) * 8
        return num % bucket_num

#找出資料是否在bucket中, 如果有return True 沒有return False
def BucketSearch(url, bucket, bucket_num):
	num = BucketHash(url, bucket_num)
	if url in bucket[num]:
		return True
	else:
		return False
'''	